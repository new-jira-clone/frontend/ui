module.exports = {
  root: true,
  env: {
    node: true,
    es2022: true,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', 'vue'],
      },
    },
  },
  extends: ['plugin:vue/vue3-recommended', '@vue/airbnb', '@vue/typescript/recommended', 'plugin:storybook/recommended', 'plugin:storybook/recommended'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': [
      0,
      {
        caseSensitive: false,
      },
    ],
    'import/no-named-as-default': [0],
    'import/no-extraneous-dependencies': [
      0,
      {
        devDependencies: ['**/*.test.js', '**/*.spec.js'],
      },
    ],
    'operator-linebreak': 'off',
    'max-len': ['error', { code: 130 }],
    'vuejs-accessibility/label-has-for': ['off'],
    'vuejs-accessibility/aria-props': ['off'],
    'vuejs-accessibility/form-control-has-label': ['off'],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    'arrow-body-style': ['off'],
    'no-shadow': 'off',
    'no-continue': 'off',
    'linebreak-style': 'off',
    'no-restricted-syntax': 'off',
    'import/export': 'off',
    'func-call-spacing': 'off',
    'no-undef': 'off',
    'no-spaced-func': 'off',
    'no-param-reassign': 'off',
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
    {
      files: ['**/icons/**.vue'],
      rules: {
        'max-len': ['off'],
      },
    },
  ],
};
