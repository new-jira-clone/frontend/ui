import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import dts from 'vite-plugin-dts';
import ViteSvgSpriteWrapper from 'vite-svg-sprite-wrapper';
import svgLoader from 'vite-svg-loader';

export default defineConfig({
  plugins: [
    vue(),
    svgLoader({
      defaultImport: 'url',
    }),
    dts({
      insertTypesEntry: true,
    }),
    ViteSvgSpriteWrapper({
      icons: 'src/icons/*.svg',
      outputDir: 'assets',
      sprite: {
        shape: {
          transform: ['svgo'],
        },
        mode: {
          symbol: {
            dest: '.',
            sprite: `icons.svg`,
          },
        },
      },
    }),
  ],
  resolve: {
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    alias: {
      '@': resolve(__dirname, 'src/'),
      '@new-jira-clone/ui/src': resolve(__dirname, 'src/'),
    },
  },
  build: {
    lib: {
      entry: resolve(__dirname, 'src/index.ts'),
      name: 'ui',
      fileName: (format) => `ui.${format}.ts`,
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
});
