import { App } from 'vue';

export interface UiOptions {
  directives?: Record<string, any>;
  svgSprite?: {
    raw: string;
  };
}

export function createUi(uiOptions: UiOptions = {}) {
  const { directives = {} } = uiOptions;

  const setIdPrefix = (app: App) => {
    app.config.idPrefix = 'ui';
  };

  const addSvgSpriteToPrependBody = (raw: string) => {
    const addSvgSprite = async () => {
      const svgDiv = document.createElement('div');

      svgDiv.setAttribute('nonce', '');
      svgDiv.style.position = 'absolute';
      svgDiv.style.pointerEvents = 'none';
      svgDiv.innerHTML = raw;

      document.body.prepend(svgDiv);
    };

    if (document.readyState === 'loading') {
      document.addEventListener('DOMContentLoaded', addSvgSprite);
    } else {
      addSvgSprite();
    }
  };

  const install = (app: App) => {
    setIdPrefix(app);

    const directiveValues = Object.values(directives);

    for (const value of directiveValues) {
      app.directive(value.directiveName, value);
    }

    if (uiOptions.svgSprite) {
      addSvgSpriteToPrependBody(uiOptions.svgSprite.raw);
    }
  };

  return {
    install,
  };
}
