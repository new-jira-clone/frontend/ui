import { DialogPlugin } from '../plugins/dialog';

export {};

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $dialog: DialogPlugin;
  }
}
