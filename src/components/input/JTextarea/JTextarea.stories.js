import { ref } from 'vue';
import JTextarea from './JTextarea.vue';

export default {
  title: 'Input/JTextarea',
  component: JTextarea,
};

const Template = (args) => ({
  components: { JTextarea },
  setup() {
    const name = ref('');

    return { args, name };
  },
  template: `
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextarea
        v-model="name"
        v-bind="args"
      />
    </div>
  `,
});

export const jTextarea = Template.bind({});
jTextarea.args = {
  placeholder: 'Placeholder',
  autoHeight: true,
};
