import { ref } from 'vue';
import JCheckbox, { sizesConfig } from './JCheckbox.vue';

export default {
  title: 'Input/JCheckbox',
  component: JCheckbox,
  size: {
    options: sizesConfig,
    control: { type: 'radio' },
  },
};

const Template = (args) => ({
  components: { JCheckbox },
  setup() {
    const isActive = ref(false);

    return { args, isActive };
  },
  template: `
  <div style="max-width: max-content;" @click.native="isActive = !isActive">
    <JCheckbox :active="isActive">
      <template #label>
        <div @click.native="isActive = !isActive">
          Checkbox label
        </div>
      </template>
    </JCheckbox>
  </div>
  `,
});

const TemplateSizes = (args) => ({
  components: { JCheckbox },
  setup() {
    const isActive = ref(false);

    return { args, isActive };
  },
  template: `
  <div style="max-width: max-content;" @click.native="isActive = !isActive">
    <JCheckbox :active="isActive">
      <template #label>
        <div @click.native="isActive = !isActive">
          xs
        </div>
      </template>
    </JCheckbox>

    <br />

    <JCheckbox :active="isActive" size="s">
      <template #label>
        <div @click.native="isActive = !isActive">
          s
        </div>
      </template>
    </JCheckbox>
  </div>
  `,
});

export const Base = Template.bind({});
Base.args = {
  active: false,
};

export const sizes = TemplateSizes.bind({});
sizes.args = {
  active: false,
};
