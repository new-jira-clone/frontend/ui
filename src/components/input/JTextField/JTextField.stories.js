import { ref } from 'vue';
import JTextField from './JTextField.vue';
import JErrorMessages from '../../error/JErrorMessages/JErrorMessages.vue';

export default {
  title: 'Input/JTextField',
  component: JTextField,
};

const Template = (args) => ({
  components: { JTextField },
  setup() {
    const name = ref('');

    return { args, name };
  },
  template: `
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextField
        v-model="name"
        v-bind="args"
      />
    </div>
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextField
        v-model="name"
        placeholder="Regular"
        dense
        text-margin
      />
    </div>
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextField
        v-model="name"
        placeholder="Outlined"
        outlined
        dense
        text-margin
      />
    </div>
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextField
        v-model="name"
        placeholder="Underlined"
        outlined
        regular
        text-margin
      />
    </div>
  `,
});

const TemplateWithError = (args) => ({
  components: { JTextField, JErrorMessages },
  setup() {
    const name = ref('');

    return { args, name };
  },
  template: `
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JTextField
        v-model="name"
        v-bind="args"
      >
        <template #info>
          <JErrorMessages :error-messages="['Input is required', 'Minimum length is 5 characters']" />
        </template>
      </JTextField>
    </div>
  `,
});

export const Base = Template.bind({});
Base.args = {
  placeholder: 'Base input',
  showInput: true,
  outlined: true,
  dense: false,
  regular: false,
  setFocus: false,
  textMargin: false,
  closeIcon: false,
};

export const TextFieldWithErrors = TemplateWithError.bind({});
TextFieldWithErrors.args = {
  placeholder: 'Base input',
  showInput: true,
  outlined: false,
  dense: true,
  regular: false,
  setFocus: false,
  textMargin: false,
  closeIcon: false,
};
