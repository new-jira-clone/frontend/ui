import { ref } from 'vue';
import JSelect from './JSelect.vue';
import JOption from '../JOption';

export default {
  title: 'Input/JSelect',
  component: JSelect,
};

const Template = (args) => ({
  components: { JSelect },
  setup() {
    const selectedUser = ref('User 1');

    const onChange = (newUsers) => {
      selectedUser.value = newUsers[0];
    };

    return { args, selectedUser, onChange };
  },
  template: `
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JSelect
        v-bind="args"
        :model-value="[selectedUser]"
        :input="{ dense: true }"
        @update:modelValue="onChange"
      >
        <div
          v-for="(userId, index) in ['User 1', 'User 2', 'User 3', 'User 4', 'User 5',]"
          :key="index"
          :value="userId"
          class="select-item"
        >
          <span class="select-item-label">
            {{ userId }}
          </span>
        </div>
      </JSelect>
    </div>
  `,
});

const TemplateMultiple = (args) => ({
  components: { JSelect, JOption },
  setup() {
    const selectedUsers = ref([1]);

    return { args, selectedUsers };
  },
  template: `
    <div style="max-width: 300px; margin-bottom: 20px;">
      <JSelect
        :modelValue="selectedUsers"
        multiple
        :input="{ regular: true }"
      >
        <JOption
          v-for="(user, index) in [1,2,3,4,5,6,7]"
          :key="index"
          :value="user"
        >
          <template #default="{ active }">
            <JCheckbox :active="active">
              <template #label>
                <span class="select-item-label">
                  User {{ user }}
                </span>
              </template>
            </JCheckbox>
          </template>
        </JOption />

        <template #result>
          <div
            v-for="(user, index) in selectedUsers"
            :key="index"
            class="selected-item"
          >
            <span style="margin-right: 8px;">
              User {{ user }}
            </span>
          </div>
        </template>
      </JSelect>
    </div>
  `,
});

export const Base = Template.bind({});
Base.args = {
  chips: false,
};

export const Multiple = TemplateMultiple.bind({});
Multiple.args = {};
