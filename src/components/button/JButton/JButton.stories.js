import JButton, { sizesConfig, typesConfig, colorsConfig } from './JButton.vue';
import JIcon from '../../icon/JIcon';

export default {
  title: 'Buttons/JButton',
  component: JButton,
  argTypes: {
    size: {
      options: sizesConfig,
      control: { type: 'radio' },
    },
    type: {
      options: typesConfig,
      control: { type: 'radio' },
    },
    color: {
      options: colorsConfig,
      control: { type: 'radio' },
    },
  },
};

const Template = (args) => ({
  components: { JButton, JIcon },
  setup() {
    return { args };
  },
  template: '<JButton v-bind="args">{{ args.text }}</JButton>',
});

const TemplateSizes = (args) => ({
  components: { JButton },
  setup() {
    return { args };
  },
  template: `
    <JButton size="xss" class="mt-2">Button xss</JButton>
    <JButton size="xs" class="mt-2">Button XS</JButton>
    <JButton size="s" class="mt-2">Button S</JButton>
    <JButton size="m" class="mt-2">Button M</JButton>
  `,
});

const TemplateTypes = (args) => ({
  components: { JButton },
  setup() {
    return { args };
  },
  template: `
    <JButton size="xs" class="mt-2">Button primary</JButton>
    <JButton size="xs" type="outlined" class="mt-2">Button outlined</JButton>
    <JButton size="xs" type="stroked" class="mt-2">Button stroked</JButton>
    <JButton size="xs" type="flat" class="mt-2">Button flat</JButton>
  `,
});

const TemplateColors = (args) => ({
  components: { JButton },
  setup() {
    return { args };
  },
  template: `
    <JButton size="xs" class="mt-2">Button primary</JButton>
    <JButton size="xs" color="white" class="mt-2">Button white</JButton>
    <JButton size="xs" color="red" class="mt-2">Button red</JButton>
  `,
});

const TemplateIcon = (args) => ({
  components: { JButton, JIcon },
  setup() {
    return { args };
  },
  template: `
    xxs: 
    <JButton icon size="xxs" class="mt-2">
      <JIcon icon="phone" size="12px" />
    </JButton>
    XS:
    <JButton icon size="xs" class="mt-2">
      <JIcon icon="phone" size="14px" />
    </JButton>
    S:
    <JButton icon size="s" color="white" class="mt-2">
      <JIcon icon="phone" size="16px" />
    </JButton>
    M: <JButton icon size="m" color="red" class="mt-2">
      <JIcon icon="phone" size="26px" />
    </JButton>
  `,
});

const TemplateSlotIcon = (args) => ({
  components: { JButton },
  setup() {
    return { args };
  },
  template: `
    <JButton v-bind="args" class="mt-2">
      <div style="display: flex; align-items: center;">
        Button primary
        <JIcon container-size size="24px" icon="copy" />
      </div>
    </JButton>
  `,
});

export const Base = Template.bind({});
Base.args = {
  text: 'I am Button',
  color: 'primary',
  size: 'xs',
  type: 'primary',
  loading: false,
};

export const sizes = TemplateSizes.bind({});
sizes.args = {};

export const types = TemplateTypes.bind({});
types.args = {};

export const colors = TemplateColors.bind({});
colors.args = {};

export const icon = TemplateIcon.bind({});
icon.args = {};

export const withSlot = TemplateSlotIcon.bind({});
withSlot.args = {
  color: 'primary',
  size: 'xs',
  type: 'primary',
  loading: false,
};
