import JAvatar from './JAvatar.vue';
import JIcon from '../../icon/JIcon';

export default {
  title: 'Avatars/JAvatar',
  component: JAvatar,
  argTypes: {
    size: {
      options: ['xs', 's', 'm', 'xxl', 'xl-3', 'full'],
      control: { type: 'radio' },
    },
    rounded: {
      options: ['full'],
      control: { type: 'radio' },
    },
    image: {
      options: [
        'https://res.cloudinary.com/doqir4zpe/image/upload/v1672093406/ironman_c3jrbc_ej99co.jpg',
        'https://res.cloudinary.com/doqir4zpe/image/upload/v1672093431/captain_e8s9nk_dkwygr.jpg',
      ],
      control: { type: 'radio' },
    },
  },
};

const Template = (args) => ({
  components: { JAvatar, JIcon },
  setup() {
    return { args };
  },
  template: `
    <JAvatar v-bind="args">
      <img :src="args.image" />

      <template v-if="args.showStatusSlot" #status>
        <JIcon icon="activeStatus" />
      </template>
    </JAvatar>
  `,
});

export const jAvatar = Template.bind({});
jAvatar.args = {
  size: 'xs',
  rounded: 'full',
  showStatusSlot: true,
  image: 'https://res.cloudinary.com/doqir4zpe/image/upload/v1672093406/ironman_c3jrbc_ej99co.jpg',
};
