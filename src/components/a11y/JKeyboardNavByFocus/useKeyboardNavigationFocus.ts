import { type Ref, ref, unref, watch } from 'vue';

export default (reverse: Ref<boolean> | boolean) => {
  const keyboardNavContainer = ref<HTMLDivElement | null>(null);
  let keyboardNavItemsCollection: HTMLCollection | null = null;
  let currentFocusIndex = 0;

  const getKeyboardNavItem = (index: number): Element | null => {
    if (keyboardNavItemsCollection === null) return null;

    return keyboardNavItemsCollection.item(index);
  };

  const setFocus = (item: Element, newFocusIndex: number) => {
    if ('focus' in item && typeof item.focus === 'function') {
      currentFocusIndex = newFocusIndex;
      item.focus();
    }
  };

  const focusOnFirstElement = () => {
    const keyboardNavItem = getKeyboardNavItem(0);

    if (keyboardNavItem === null) return;

    setFocus(keyboardNavItem, 0);
  };

  const focusOnLastElement = () => {
    if (keyboardNavItemsCollection === null) return null;

    const keyboardNavItem = getKeyboardNavItem(keyboardNavItemsCollection.length - 1);

    if (keyboardNavItem === null) return;

    setFocus(keyboardNavItem, keyboardNavItemsCollection.length - 1);
  };

  const focusOnNextElement = () => {
    const keyboardNavItem = getKeyboardNavItem(currentFocusIndex + 1);

    if (keyboardNavItem === null) return;

    setFocus(keyboardNavItem, currentFocusIndex + 1);
  };

  const focusOnPrevElement = () => {
    const keyboardNavItem = getKeyboardNavItem(currentFocusIndex - 1);

    if (keyboardNavItem === null) return;

    setFocus(keyboardNavItem, currentFocusIndex - 1);
  };

  const setInitialFocus = () => {
    if (unref(reverse)) {
      focusOnLastElement();
    } else {
      focusOnFirstElement();
    }
  };

  watch(keyboardNavContainer, () => {
    if (!keyboardNavContainer.value) return;

    keyboardNavItemsCollection =
      keyboardNavContainer.value.getElementsByClassName('j-keyboard-nav-item');
  });

  return {
    keyboardNavContainer,
    focusOnNextElement,
    focusOnPrevElement,
    setInitialFocus,
  };
};
