import JKeyboardNavByFocus from './JKeyboardNavByFocus.vue';
import JKeyboardNavItem from './JKeyboardNavItem';
import JList from '../../list/JList';
import JListItem from '../../list/JListItem';

export default {
  title: 'A11y/JKeyboardNavByFocus',
};

const Template = (args) => ({
  components: { JKeyboardNavByFocus, JKeyboardNavItem, JList, JListItem },
  setup() {
    return {
      args,
    };
  },
  template: `
    Set focus:
    <div style="width: 250px;">
      <JKeyboardNavByFocus v-bind="args">
        <template #default="keyboardNavProps">
          <JList
            v-bind="keyboardNavProps"
            style="background-color: rgb(238, 238, 238); width: 250px;"
          >
            <JKeyboardNavItem
              v-for="index in 10"
              :key="index"
            >
              <template #default="keyboardItemProps">
                <JListItem
                  v-bind="keyboardItemProps"
                  size="m"
                >
                  Item {{ index }}
                </JListItem>
              </template>
            </JKeyboardNavItem>
          </JList>
        </template>
      </JKeyboardNavByFocus>
    </div>
  `,
});

export const jKeyboardNavByFocus = Template.bind({});
jKeyboardNavByFocus.args = {
  reverse: false,
};
