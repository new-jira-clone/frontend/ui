import JKeyboardNavHint from './JKeyboardNavHint.vue';
import JTopRightKeyboardNavHint from './JTopRightKeyboardNavHint.vue';

export { JKeyboardNavHint, JTopRightKeyboardNavHint };
export default JKeyboardNavHint;
