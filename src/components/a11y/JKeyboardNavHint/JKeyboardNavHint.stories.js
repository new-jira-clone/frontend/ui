import JKeyboardVerticalNavHint from './JKeyboardVerticalNavHint.vue';
import JKeyboardNavHint from './JKeyboardNavHint.vue';

export default {
  title: 'A11y/JKeyboardVerticalNavHint',
};

const Template = (args) => ({
  components: { JKeyboardVerticalNavHint, JKeyboardNavHint },
  setup() {
    return {
      args,
    };
  },
  template: `
    <div>
      All directions
      <JKeyboardNavHint />

      <br />

      Top/Bottom
      <JKeyboardVerticalNavHint v-bind="args" />
    </div>
  `,
});

export const jKeyboardVerticalNavHint = Template.bind({});
jKeyboardVerticalNavHint.args = {};
