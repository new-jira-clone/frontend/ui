import { MethodInsertTypes } from '../types/MethodInsertTypes';

export interface InsertPosition {
  order: number | null,
  method: MethodInsertTypes | null,
}
