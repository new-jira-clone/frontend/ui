import JDraggableBoard from './JDraggableBoard.vue';
import JDraggable from './JDraggable.vue';

export { JDraggableBoard, JDraggable };
export default JDraggableBoard;
