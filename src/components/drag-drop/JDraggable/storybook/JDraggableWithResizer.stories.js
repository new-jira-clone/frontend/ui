import ResizerDraggableTemplate from './ResizerDraggableTemplate.vue';
import JResizerStory from '@new-jira-clone/ui/src/components/shared/JResizer/JResizer.stories.js';

export default {
  title: 'drag-drop/JDraggable',
  argTypes: {
    ...JResizerStory.argTypes,
  },
  args: {
    ...JResizerStory.args,
  },
};

const TemplateWithResizer = (args) => ({
  components: { ResizerDraggableTemplate },
  setup() {
    return {
      args,
    };
  },
  template: `
    <ResizerDraggableTemplate v-bind="args" />
  `,
});

export const WithResizer = TemplateWithResizer.bind({});
