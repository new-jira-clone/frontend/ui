import BaseDraggableTemplate from './BaseDraggableTemplate.vue';

export default {
  title: 'drag-drop/JDraggable',
};

const Template = (args) => ({
  components: { BaseDraggableTemplate },
  setup() {
    return {
      args,
    };
  },
  template: `
    <BaseDraggableTemplate v-bind="args" />
  `,
});

export const base = Template.bind({});
