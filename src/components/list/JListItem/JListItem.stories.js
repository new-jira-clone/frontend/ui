import JListItem, { sizeConfig } from './JListItem.vue';

export default {
  title: 'Shared/List/Item',
  component: JListItem,
  argTypes: {
    size: {
      options: sizeConfig,
      control: { type: 'select' },
    },
  },
  args: {
    size: 'l',
    tag: 'div',
    active: false,
    disabled: false,
    hoverEffect: true,
    hoverOnIcons: true,
  },
};

const TemplateWithResizer = (args) => ({
  components: { JListItem },
  setup() {
    return {
      args,
    };
  },
  template: `
    <JListItem v-bind="args" style="background-color: rgb(238, 238, 238); width: 250px;">
      List item
    </JListItem>
  `,
});

export const Item = TemplateWithResizer.bind({});
