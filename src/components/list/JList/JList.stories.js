import JList from './JList.vue';
import JListItem from '../JListItem/JListItem.vue';
import JListStory from '../JListItem/JListItem.stories';

export default {
  title: 'Shared/List',
  component: JList,
  subcomponents: { JListItem },
  argTypes: JListStory.argTypes,
  args: JListStory.args,
};

const TemplateWithResizer = (args) => ({
  components: { JList, JListItem },
  setup() {
    return {
      args,
    };
  },
  template: `
    <JList style="background-color: rgb(238, 238, 238); width: 250px;">
      <JListItem v-bind="args" v-for="index in 5" :key="index">
        <JIcon icon="chat" class="mr-2" />
        List item {{ index }}
      </JListItem>
    </JList>
  `,
});

export const List = TemplateWithResizer.bind({});
