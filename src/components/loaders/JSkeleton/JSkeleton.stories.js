import JSkeleton from './JSkeleton.vue';

export default {
  title: 'Loaders/JSkeleton',
};

const Template = (args) => ({
  components: { JSkeleton },
  setup() {
    return {
      args,
    };
  },
  template: `
    <div>
      <JSkeleton v-bind="args" />
    </div>
  `,
});

export const jSkeleton = Template.bind({});
jSkeleton.args = {
  width: '400px',
  height: '100px',
}
