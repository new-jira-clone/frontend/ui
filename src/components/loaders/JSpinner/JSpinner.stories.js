import JSpinner from './JSpinner.vue';

export default {
  title: 'Loaders/JSpinner',
};

const Template = (args) => ({
  components: { JSpinner },
  setup() {
    return {
      args,
    };
  },
  template: `
    <div>
      <JSpinner v-bind="args" />
    </div>
  `,
});

export const jSpinner = Template.bind({});
jSpinner.args = {
  width: '30px',
  height: '30px',
}
