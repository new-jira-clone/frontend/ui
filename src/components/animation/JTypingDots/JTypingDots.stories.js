import JTypingDots from './JTypingDots.vue';

export default {
  title: 'Animation/JTypingDots',
  component: JTypingDots,
};

const Template = (args) => ({
  components: { JTypingDots },
  setup() {
    return { args };
  },
  template: `
    <JTypingDots />
  `,
});

export const jTypingDots = Template.bind({});
jTypingDots.args = {};
