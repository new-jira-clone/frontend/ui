import { nextTick } from 'vue';
import JMenu from '../JMenu.vue';
import './styles.scss';

export default {
  title: 'Overlay/JMenu',
  argTypes: {
    position: {
      options: ['below'],
      control: { type: 'radio' },
    },
  },
  args: {
    devBorder: true,
  },
};

const Template = (args) => ({
  components: { JMenu },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; justify-content: space-between; margin-top: 100px;">
      <JMenu v-bind="args">
        <template #activator="menuSlotProps">
          <JButton v-bind="menuSlotProps">
            Activator Top Left
          </JButton>
        </template>
        <JList>
          <JListItem>List item 1</JListItem>
          <JListItem>List item 2</JListItem>
          <JListItem>List item 3</JListItem>
        </JList>
      </JMenu>
      <JMenu v-bind="args">
        <template #activator="menuSlotProps">
          <div>
            <JButton v-bind="menuSlotProps" >
              Activator Top Right
            </JButton>
          </div>
        </template>
        <JList>
          <JListItem>List item 1</JListItem>
          <JListItem>List item 2</JListItem>
          <JListItem>List item 3</JListItem>
        </JList>
      </JMenu>
    </div>
    <div style="position: absolute; bottom: 20px;">
      <JMenu v-bind="args">
        <template #activator="menuSlotProps">
          <div>
            <JButton v-bind="menuSlotProps" >
              Activator Left bottom
            </JButton>
          </div>
        </template>
        <JList>
          <JListItem>List item 1</JListItem>
          <JListItem>List item 2</JListItem>
          <JListItem>List item 3</JListItem>
        </JList>
      </JMenu>
    </div>
    <div style="position: absolute; bottom: 20px; right: 16px;">
      <JMenu v-bind="args">
        <template #activator="menuSlotProps">
          <div>
            <JButton v-bind="menuSlotProps" >
              Activator Left bottom
            </JButton>
          </div>
        </template>
        <JList>
          <JListItem>List item 1</JListItem>
          <JListItem>List item 2</JListItem>
          <JListItem>List item 3</JListItem>
        </JList>
      </JMenu>
    </div>
  `,
});

const TemplateWithList = (args) => ({
  components: { JMenu },
  setup() {
    return { args };
  },
  template: `
    <JMenu v-bind="args">
      <template #activator="menuSlotProps">
        <div style="width: 200px; margin-left: 100px; margin-top: 100px;">
          <JList style="background-color: #eee;">
            <JListItem v-for="index in 3" :key="index" v-bind="menuSlotProps">Open new menu {{ index }}</JListItem>
          </JList>
        </div>
      </template>
      <JList>
        <JListItem>
          <JIcon icon="check" />
          List item 1
        </JListItem>
        <JListItem>List item 2</JListItem>
        <JListItem>List item 3</JListItem>
      </JList>
    </JMenu>
  `,
});

const TemplateNestingMenu = (args) => ({
  components: { JMenu },
  setup() {
    const onUpdateValue = async (value) => {
      if (value === false) return;

      await nextTick();

      const menus = document.querySelectorAll('.j-menu-wrapper');

      if (args.devBorder) {
        menus.forEach((menu) => {
          menu.classList.add('dev-border');
        });
      } else {
        menus.forEach((menu) => {
          menu.classList.remove('dev-border');
        });
      }
    };

    return { args, onUpdateValue };
  },
  template: `
    <JMenu v-bind="args" @update:model-value="onUpdateValue">
      <template #activator="menuSlotProps">
        <div style="width: 200px; margin-left: 100px; margin-top: 100px;">
          <div style="background-color: #eee;">
            <JListItem v-for="index in 3" :key="index" v-bind="menuSlotProps">Open new menu {{ index }}</JListItem>
          </div>
        </div>
      </template>

      <template #default="nestingProps">
        <JMenu v-bind="{...args, ...nestingProps}" @update:model-value="onUpdateValue">
          <template #activator="menuSlotProps2">
            <JList>
              <JListItem v-bind="menuSlotProps2">
                <JIcon icon="check" />
                List item 1
              </JListItem>
            </JList>
          </template>

          <template #default="nestNestingProps">
            <JMenu v-bind="{...args, ...nestNestingProps}" @update:model-value="onUpdateValue">
              <template #activator="menuSlotProps3">
                <JList>
                  <JListItem v-for="index in 5" :key="index" v-bind="menuSlotProps3">
                    <span>Nesting List item {{ index }}</span>
                  </JListItem>
                </JList>
              </template>

              <template #default>
                <JList>
                  <JListItem v-for="index in 5" :key="index">
                    <span>Nesting List item {{ index }}</span>
                  </JListItem>
                </JList>
              </template>
            </JMenu>
          </template>
        </JMenu>
      </template>
    </JMenu>
  `,
});

export const OpenByLeftClick = Template.bind({});
OpenByLeftClick.args = {
  openByRightClick: false,
  dynamicPosition: false,
  closeOnClick: false,
  openOnHover: false,
  autoWidth: false,
  position: 'below',
};

export const OpenByRightClick = Template.bind({});
OpenByRightClick.args = {
  openByRightClick: true,
  dynamicPosition: false,
  closeOnClick: true,
  openOnHover: false,
  autoWidth: false,
  position: 'below',
};

export const OpenMenuByMousePosition = Template.bind({});
OpenMenuByMousePosition.args = {
  openByRightClick: false,
  dynamicPosition: true,
  closeOnClick: true,
  openOnHover: false,
  autoWidth: false,
  position: 'below',
};

export const OpenMenuWithList = TemplateWithList.bind({});
OpenMenuWithList.args = {
  openByRightClick: true,
  dynamicPosition: true,
  closeOnClick: true,
  openOnHover: false,
  autoWidth: true,
  position: 'below',
};

export const NestingMenu = TemplateNestingMenu.bind({});
NestingMenu.args = {
  openByRightClick: false,
  dynamicPosition: true,
  closeOnClick: true,
  openOnHover: true,
  autoWidth: true,
  position: 'below',
};
