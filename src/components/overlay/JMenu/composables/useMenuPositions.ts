import { ref } from 'vue';

export type DeviceEvent = MouseEvent | PointerEvent;

interface MenuPositionOptions {
  autoWidth: boolean;
  dynamicPosition: boolean;
  attach: string | boolean;
  pageX?: number;
  pageY?: number;
}

export default () => {
  const menuRef = ref<HTMLElement | null>(null);
  const activatorRef = ref<HTMLElement | null>(null);

  const setMenuPositions = (
    activatorRect: DOMRect,
    menuRect: DOMRect,
    options: MenuPositionOptions,
  ): void => {
    if (menuRef.value === null || options.attach === false) return;

    // TODO: давнее решение, вспомнить в чем минусы и если что переписать на top/left + 20% (scale 0.8);
    const menuWidth = options.autoWidth ? Math.max(activatorRect.width, menuRect.width) : menuRect.width;
    const intialScale = 1.2;
    const documentWidth = document.documentElement.clientWidth;
    const documentHeight = document.documentElement.clientHeight;
    const isMenuOutsideBoundaryX = activatorRect.right + menuWidth * intialScale > documentWidth;
    const isMenuOutsideBoundaryY = activatorRect.bottom + menuRect.height * intialScale > documentHeight;

    let transformOrigin = '';

    if (options.dynamicPosition && options.pageX && options.pageY) {
      if (isMenuOutsideBoundaryY) {
        menuRef.value.style.bottom = `${documentHeight - options.pageY}px`;
        transformOrigin = 'bottom';
      } else {
        menuRef.value.style.top = `${options.pageY}px`;
        transformOrigin = 'top';
      }

      if (isMenuOutsideBoundaryX) {
        menuRef.value.style.right = `${documentWidth - options.pageX}px`;
        transformOrigin += ' right';
      } else {
        menuRef.value.style.left = `${options.pageX}px`;
        transformOrigin += ' left';
      }

      menuRef.value.style.transformOrigin = transformOrigin;

      return;
    }

    if (isMenuOutsideBoundaryY) {
      menuRef.value.style.bottom = `${documentHeight - activatorRect.top}px`;
      transformOrigin = 'bottom';
    } else {
      menuRef.value.style.top = `${activatorRect.bottom}px`;
      transformOrigin = 'top';
    }

    if (isMenuOutsideBoundaryX) {
      menuRef.value.style.right = `${documentWidth - activatorRect.right}px`;
      transformOrigin += ' right';
    } else {
      menuRef.value.style.left = `${activatorRect.left}px`;
      transformOrigin += ' left';
    }

    menuRef.value.style.transformOrigin = transformOrigin;
  };

  const setMenuRect = async (options: MenuPositionOptions): Promise<void> => {
    if (activatorRef.value === null || menuRef.value === null) return;

    const menuCoords: DOMRect = menuRef.value.getBoundingClientRect();
    const activatorCoords = activatorRef.value.getBoundingClientRect();
    const boxshadowOffset = 20;

    if (options.autoWidth) {
      menuRef.value.style.minWidth = `${activatorCoords.width}px`;
    }

    setMenuPositions(activatorCoords, menuCoords, options);

    if (menuCoords.bottom + boxshadowOffset > window.innerHeight) {
      const menuMaxHeight =
        menuCoords.height - (menuCoords.bottom - window.innerHeight) - boxshadowOffset;

      menuRef.value.style.maxHeight = `${menuMaxHeight}px`;
    }
  };

  const isMouseOutByMenu = (event: DeviceEvent, menuCoords: DOMRect): boolean => {
    if (
      event.pageX > Math.floor(menuCoords.right) + 80 ||
      event.pageX < Math.floor(menuCoords.left) - 80 ||
      event.pageY < Math.floor(menuCoords.top) - 80 ||
      event.pageY > Math.floor(menuCoords.bottom) + 80
    ) {
      return true;
    }

    return false;
  };

  const isHideMenuIfMouseOutByActivator = (activator: HTMLElement) => {
    const activatorCoords = activator.getBoundingClientRect();
    let isShowMenuOnHover = true;

    return (event: DeviceEvent) => {
      if (
        event.pageX - 1 > Math.floor(activatorCoords.right) ||
        event.pageX + 1 < Math.floor(activatorCoords.left) ||
        event.pageY + 1 < Math.floor(activatorCoords.top) ||
        event.pageY - 1 > Math.floor(activatorCoords.bottom)
      ) {
        isShowMenuOnHover = false;
      }

      return isShowMenuOnHover;
    };
  };

  const getActualDeviceEvent = () => {
    let actualEvent: DeviceEvent | null = null;

    return (event: DeviceEvent | null): DeviceEvent | null => {
      if (event === null) {
        const temporary = actualEvent;
        actualEvent = null;
        return temporary;
      }

      actualEvent = event;
      return actualEvent;
    };
  };

  return {
    menuRef,
    activatorRef,
    setMenuRect,
    getActualDeviceEvent,
    isMouseOutByMenu,
    isHideMenuIfMouseOutByActivator,
  };
};
