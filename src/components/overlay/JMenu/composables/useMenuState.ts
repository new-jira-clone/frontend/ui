import { ref } from 'vue';

type Menu = {
  id: number;
  parentId: number | null;
  childId?: number;
  callback?: any;
  isHide: boolean;
  isOpen: boolean;
};

const globalMenuId = ref(0);
const openedMenus: { [key: number]: Menu } = {};
const idsElemenets = ref<{ [key: number]: HTMLElement }>({});

export default () => {
  const increaseMenuId = (): void => {
    globalMenuId.value += 1;
  };

  const getNewMenuId = () => {
    increaseMenuId();

    return globalMenuId.value;
  };

  const setIdAndMenuElement = (id: number, menu: HTMLElement): void => {
    idsElemenets.value[id] = menu;
  };

  const removeIdAndMenu = (id: number): void => {
    delete idsElemenets.value[id];
  };

  const addMenu = (menuId: number, parentId: number | null): void => {
    const menu: Menu = {
      id: menuId,
      parentId,
      isHide: false,
      isOpen: true,
    };

    if (parentId) {
      openedMenus[parentId].childId = menu.id;
    }

    openedMenus[menuId] = menu;
  };

  const removeMenu = (id: number): void => {
    delete openedMenus[id];
  };

  const isHasChild = (id: number): boolean => {
    if (!openedMenus[id]) return false;

    return (
      !!openedMenus[id].childId &&
      openedMenus[openedMenus[id].childId] &&
      openedMenus[openedMenus[id].childId].isOpen
    );
  };

  const isHiddenChildren = (id: number): boolean => {
    if (openedMenus[id].isHide === false) return false;

    if (openedMenus[id].childId) {
      return isHiddenChildren(openedMenus[id].childId);
    }

    return true;
  };

  const setCallback = (id: number, callback: any): void => {
    if (openedMenus[id]) {
      openedMenus[id].callback = callback;
    }
  };

  const setHideValue = (id: number, isHide: boolean) => {
    if (openedMenus[id]) {
      openedMenus[id].isHide = isHide;
    }
  };

  const runChainNestingMenu = (id: number) => {
    if (openedMenus[id].parentId && openedMenus[openedMenus[id].parentId].callback) {
      openedMenus[openedMenus[id].parentId].callback();

      runChainNestingMenu(openedMenus[id].parentId);
    }
  };

  const getChildById = (id: number): number => {
    return openedMenus[id]?.childId ?? -1;
  };

  const getParentElements = (id: number, elements: HTMLElement[] = []): HTMLElement[] => {
    if (!openedMenus[id]) return elements;
    if (idsElemenets.value[id]) {
      elements.push(idsElemenets.value[id]);
    }

    if (openedMenus[id].parentId) {
      return getParentElements(openedMenus[id].parentId, elements);
    }

    return elements;
  };

  const getChildElements = (id: number, elements: HTMLElement[] = []): HTMLElement[] => {
    if (!openedMenus[id]) return elements;
    if (idsElemenets.value[id]) {
      elements.push(idsElemenets.value[id]);
    }

    if (openedMenus[id].childId) {
      return getParentElements(openedMenus[id].childId, elements);
    }

    return elements;
  };

  return {
    addMenu,
    removeMenu,
    getNewMenuId,
    setHideValue,
    isHasChild,
    setCallback,
    isHiddenChildren,
    runChainNestingMenu,
    setIdAndMenuElement,
    removeIdAndMenu,
    idsElemenets,
    getParentElements,
    getChildElements,
    getChildById,
  };
};
