import JResizerStory from '@new-jira-clone/ui/src/components/shared/JResizer/JResizer.stories';
import JDialog from '../JDialog.vue';
import DialogNonModalContent from './DialogNonModalContent.vue';

export default {
  title: 'Overlay/JDialog/Non Modal',
  argTypes: {
    ...JResizerStory.argTypes,
  },
  args: {
    ...JResizerStory.args,
  },
};

const Template = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
      DialogNonModalContent,
    };
  },
  template: `
    <h3 class="mb-2">Slot activator:</h3>
    <JButton @click="$dialog.show(DialogNonModalContent, { contentProps: args })">
      Open dialog
    </JButton>
  `,
});

const TemplateWithRandomId = (args) => ({
  components: { JDialog },
  setup() {
    const getRandomExternalId = () => {
      const randomId = Math.floor(Math.random() * 10);

      if (randomId < 3) {
        return '1';
      }

      if (randomId > 7) {
        return '2';
      }

      return '3';
    };

    return {
      args,
      DialogNonModalContent,
      getRandomExternalId,
    };
  },
  template: `
    <h3 class="mb-2">Slot activator:</h3>
    <JButton @click="$dialog.show(DialogNonModalContent, { contentProps: args, externalId: getRandomExternalId() })">
      Open dialog
    </JButton>
  `,
});

export const DragAndResize = Template.bind({});
export const ShowTheSameDialog = TemplateWithRandomId.bind({});
