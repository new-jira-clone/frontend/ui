import DialogBtn1 from './DialogBtn1.vue';
import DialogBtn2 from './DialogBtn2.vue';
import DialogConfirmContent from './DialogConfirmContent.vue';
import DialogWithProps from './DialogWithProps.vue';
import JDialog from '../JDialog.vue';
import JMenu from '../../JMenu';

export default {
  title: 'Overlay/JDialog/Modal',
};

const ActivatorTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
    };
  },
  template: `
    <h3 class="mb-2">Simple slot activator:</h3>
    <JDialog>
      <template #activator="slotProps">
        <JButton v-bind="slotProps">Dialog slot activator</JButton>
      </template>

      <h1>This has been activator</h1>
    </JDialog>
  `,
});

const DynamicTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
      DialogBtn2,
    };
  },
  template: `
    <h3 class="mb-2">Dynamic dialog:</h3>
    <JButton @click="$dialog.showModal(DialogBtn2)">Dynamically open</JButton>
  `,
});

const ActivatorNestingTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
    };
  },
  template: `
    <JDialog>
      <template #activator="slotProps">
        <h3 class="mb-2">Activator nestings Dialogs:</h3>
        <JButton v-bind="slotProps" class="mt-2">Show activator nesting dialog</JButton>
      </template>

      <JDialog>
        <template #activator="nestingSlotProps">
          <h2>Lorem ipsum dolor sit amet</h2>
          <JButton v-bind="nestingSlotProps" class="mt-2">Nesting activator</JButton>
        </template>

        <h2>Nesting Dialog content</h2>
      </JDialog>
    </JDialog>
  `,
});

const DynamicNestingTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
      DialogBtn1,
    };
  },
  template: `
    <h3 class="mb-2">Dynamic Dialogs:</h3>
    <JButton @click="$dialog.showModal(DialogBtn1)">Show dynamic Dialog</JButton>
  `,
});

const ConfirmTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
      DialogConfirmContent,
    };
  },
  template: `
    <h3 class="mb-2">Dialog with confirm</h3>
    <JButton @click="$dialog.showModal(DialogConfirmContent)">Dialog confirm content</JButton>
  `,
});

const DialogWithPropsTemplate = (args) => ({
  components: { JDialog },
  setup() {
    return {
      args,
      DialogWithProps,
    };
  },
  template: `
    <h3 class="mb-2">Dialog with props</h3>
    <JButton @click="$dialog.showModal(DialogWithProps, { contentProps: { title: args.propsToDialog } })">
      Dialog props content
    </JButton>
  `,
});

const DialogWithMenuTemplate = (args) => ({
  components: { JDialog, JMenu },
  setup() {
    return {
      args,
      DialogWithProps,
    };
  },
  template: `
    <h3 class="mb-2">Dialog with nesting menu</h3>
    <JDialog>
      <template #activator="slotProps">
        <JButton v-bind="slotProps">
          Dialog activator
        </JButton>
      </template>

      <JMenu>
        <template #activator="menuProps">
          <JButton v-bind="menuProps">
            Open nesting menu
          </JButton>
        </template>

        <h1>Hello, i am menu!</h1>
      </JMenu>
    </JDialog>
  `,
});

export const Activator = ActivatorTemplate.bind({});
export const Dynamic = DynamicTemplate.bind({});

export const ActivatorNestingDialogs = ActivatorNestingTemplate.bind({});
export const DynamicNestingDialogs = DynamicNestingTemplate.bind({});
export const ConfirmDialog = ConfirmTemplate.bind({});
export const PropsToDialog = DialogWithPropsTemplate.bind({});
PropsToDialog.args = {
  propsToDialog: 'Hello World, i am Props!',
};
export const DialogWithMenu = DialogWithMenuTemplate.bind({});
