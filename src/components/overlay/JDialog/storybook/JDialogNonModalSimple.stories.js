import { ref } from 'vue';
import JDialog from '../JDialog.vue';
import DialogNonModalContent from './DialogNonModalContent.vue';

export default {
  title: 'Overlay/JDialog/Non Modal',
};

const Template = (args) => ({
  components: { JDialog, DialogNonModalContent },
  setup() {
    const isOpen = ref(false);

    return {
      args,
      isOpen,
    };
  },
  template: `
    <h3 class="mb-2">Slot activator:</h3>
    <JDialog type="nonModal" v-model="isOpen">
      <template #activator="slotProps">
        <JButton v-bind="slotProps">Dialog slot activator</JButton>
      </template>

      <DialogNonModalContent v-bind="args" @close="isOpen = false" />
    </JDialog>
  `,
});

export const Activator = Template.bind({});
