import JDialog from './JDialog.vue';
import JDialogConfirm from './JDialogConfirm.vue';

export { JDialog, JDialogConfirm };
export default JDialog;
