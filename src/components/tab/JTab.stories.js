import { ref } from 'vue';
import JTabsHeader from './JTabsHeader';
import JTabHeaderItem from './JTabHeaderItem';
import JTabsBody from './JTabsBody';
import JTabBodyItem from './JTabBodyItem';

export default {
  title: 'Tabs/JTab',
  component: JTabsHeader,
};

const Template = (args) => ({
  components: { JTabsHeader, JTabHeaderItem, JTabsBody, JTabBodyItem },
  setup() {
    const activeTab = ref(0);

    return { args, activeTab };
  },
  template: `
    <div style="max-width: 456px">
      <JTabsHeader v-model="activeTab">
        <JTabHeaderItem>Tab</JTabHeaderItem>
        <JTabHeaderItem>Tab 2</JTabHeaderItem>
        <JTabHeaderItem>Tab 3 fill for to all line</JTabHeaderItem>
        <JTabHeaderItem>Tab 4</JTabHeaderItem>
      </JTabsHeader>

      <JTabsBody
        v-model="activeTab"
      >
        <JTabBodyItem>
          <h3 class="home-tab-body-title">
            Last year
          </h3>
        </JTabBodyItem>
        <JTabBodyItem>
          <h3 class="home-tab-body-title">
            Last monthly
          </h3>
        </JTabBodyItem>
        <JTabBodyItem>
          <h3 class="home-tab-body-title">
            Last weekly
          </h3>
        </JTabBodyItem>
        <JTabBodyItem>
          <h3 class="home-tab-body-title">
            Last weekly
          </h3>
        </JTabBodyItem>
      </JTabsBody>
    </div>
  `,
});

export const jTab = Template.bind({});
jTab.args = {};
