import JAppLogo from './JAppLogo.vue';

export default {
  title: 'Logo/App',
};

const Template = () => ({
  components: { JAppLogo },
  template: `
  <JAppLogo />
  `,
});

export const jAppLogo = Template.bind({});
