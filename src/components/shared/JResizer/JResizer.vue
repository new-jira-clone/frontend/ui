<script lang="ts">
import {
  computed, defineComponent, ref, onMounted, getCurrentInstance,
} from 'vue';
import JIcon from '@new-jira-clone/ui/src/components/icon/JIcon';
import JSemanticButton from '@new-jira-clone/ui/src/components/button/JSemanticButton';
import {
  allowSelection,
  forbidSelection,
} from '@new-jira-clone/ui/src/composables/useSelectionControl';
import useKeydownEventOffsetStep from '@new-jira-clone/ui/src/composables/utils/useKeydownEventOffsetStep';

const clickEmitName = 'click' as string;

type ResizeDirection = 'x' | 'y' | 'xy';

export default defineComponent({
  name: 'JResizer',
  components: {
    JIcon,
    JSemanticButton,
  },
  props: {
    minResize: {
      type: Boolean,
      default: false,
    },
    resizeBtn: {
      type: Boolean,
      default: true,
    },
    axisX: {
      type: Boolean,
      default: true,
    },
    axisY: {
      type: Boolean,
      default: false,
    },
    hideStrip: {
      type: Boolean,
      default: false,
    },
  },
  emits: [clickEmitName],
  setup(props) {
    const isntance = getCurrentInstance();

    const defaultSlot = ref<HTMLElement | null>(null);
    const isResizing = ref(false);
    const containerSize = {
      width: 0,
      height: 0,
    };
    const startMouse = {
      x: 0,
      y: 0,
    };
    const resizeDirection = ref<ResizeDirection>('x');

    const isXDir = computed(() => resizeDirection.value.includes('x'));
    const isYDir = computed(() => resizeDirection.value.includes('y'));

    const { getOffsetStepOnKeyboardEvent, resetKeydownState } = useKeydownEventOffsetStep();

    const setSize = async (
      block: HTMLElement,
      x: number | null,
      y: number | null,
    ): Promise<void> => {
      if (!block) return;

      if (x) {
        block.style.width = `${x}px`;
      }

      if (y) {
        block.style.height = `${y}px`;
      }
    };

    const keepResizing = (event: MouseEvent): void => {
      if (isResizing.value === false) return;

      const offsetX = isXDir.value ? containerSize.width + event.clientX - startMouse.x : null;
      const offsetY = isYDir.value ? containerSize.height + event.clientY - startMouse.y : null;

      setSize(defaultSlot.value, offsetX, offsetY);
    };

    const stopResizing = (): void => {
      allowSelection(resizeDirection.value);
      isResizing.value = false;

      document.removeEventListener('mouseup', stopResizing);
      document.removeEventListener('blur', stopResizing);
      document.removeEventListener('mousemove', keepResizing);
    };

    const setEventListeners = (): void => {
      document.addEventListener('mouseup', stopResizing);
      document.addEventListener('blur', stopResizing);
      document.addEventListener('mousemove', keepResizing);
    };

    const beginResizing = (event: MouseEvent, direction: ResizeDirection): void => {
      resizeDirection.value = direction;

      if (defaultSlot.value) {
        const slotStyles = window.getComputedStyle(defaultSlot.value);

        startMouse.x = event.clientX;
        startMouse.y = event.clientY;
        containerSize.width = parseInt(slotStyles.width, 10);
        containerSize.height = parseInt(slotStyles.height, 10);
      }

      isResizing.value = true;

      forbidSelection(resizeDirection.value);
      setEventListeners();
    };

    const isForbiddenDirectionY = (code: string) => {
      if (code !== 'ArrowUp' && code !== 'ArrowDown') return true;

      return !props.axisY;
    }

    const isForbiddenDirectionX = (code: string) => {
      if (code !== 'ArrowLeft' && code !== 'ArrowRight') return true;

      return !props.axisX;
    }

    const onKeydown = async (event: KeyboardEvent) => {
      if (!defaultSlot.value) return;
      if (isForbiddenDirectionY(event.code) && isForbiddenDirectionX(event.code)) return;

      // TODO: refactor getComputedStyle for optimization
      const slotStyles = window.getComputedStyle(defaultSlot.value);

      containerSize.width = parseInt(slotStyles.width, 10);
      containerSize.height = parseInt(slotStyles.height, 10);

      const [width, height] = getOffsetStepOnKeyboardEvent(event);

      setSize(defaultSlot.value, containerSize.width + width, containerSize.height + height);
    }

    onMounted(() => {
      // TODO: refactor to using useParseNodes composable
      const resizerChild = isntance?.subTree?.children?.[0]?.children;

      if (!resizerChild) return;

      const resizeContentContainer = resizerChild.find((child) => child.props?.resizeContainer);

      if (!resizeContentContainer) {
        console.warn('A JResizer component require 1 child slots');
      } else {
        defaultSlot.value = resizeContentContainer.el as HTMLElement;
      }
    });

    return {
      isXDir,
      isYDir,
      isResizing,
      beginResizing,
      onKeydown,
      resetKeydownState,
      clickEmitName,
      resizeDirection,
    };
  },
});
</script>

<template>
  <div class="j-resizer-container">
    <slot :resize-container="true" />

    <div
      v-if="axisX"
      class="j-resizer"
      :class="{ active: isResizing && isXDir, 'j-resizer-x': axisX }"
      @mousedown="beginResizing($event, 'x')"
      @keydown="onKeydown"
      @keyup="resetKeydownState"
    >
      <JSemanticButton
        v-if="resizeBtn && axisY === false"
        class="j-resizer-btn"
        :class="{ 'j-min-resize': minResize }"
        @click.stop="$emit(clickEmitName)"
      >
        <JIcon
          icon="downArrow"
          size="24px"
        />
      </JSemanticButton>
      <div v-if="hideStrip === false" class="strip" />
      <div class="line" />
    </div>

    <div
      v-if="axisY"
      class="j-resizer"
      :class="{ active: isResizing && isYDir, 'j-resizer-y': axisY }"
      @mousedown="beginResizing($event, 'y')"
      @keydown="onKeydown"
      @keyup="resetKeydownState"
    >
      <JSemanticButton
        v-if="resizeBtn && axisX === false"
        class="j-resizer-btn"
        :class="{ 'j-min-resize': minResize }"
        @click.stop="$emit(clickEmitName)"
      >
        <JIcon
          icon="downArrow"
          size="24px"
        />
      </JSemanticButton>
      <div v-if="hideStrip === false" class="strip" />
      <div class="line" />
    </div>

    <div
      v-if="axisX && axisY"
      class="j-resizer"
      :class="{
        active: isResizing && isXDir && isYDir,
        'j-resizer-xy': axisX && axisY,
      }"
      @mousedown="beginResizing($event, 'xy')"
      @keydown="onKeydown"
      @keyup="resetKeydownState"
    >
      <JSemanticButton
        v-if="resizeBtn"
        class="j-resizer-btn"
        :class="{ 'j-min-resize': minResize }"
      >
        <JIcon
          icon="downArrow"
          size="24px"
        />
      </JSemanticButton>
    </div>
  </div>
</template>

<style lang="scss">
:root {
  --resizer-size: 24px;
}

.forbid-selection {
  &.x {
    cursor: ew-resize;
  }

  &.y {
    cursor: n-resize;
  }

  &.xy {
    cursor: nw-resize;
  }
}
</style>

<style lang="scss" scoped>
@use './styles';
</style>
