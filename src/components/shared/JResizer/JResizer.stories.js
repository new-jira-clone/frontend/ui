import ResizerStoryTemplate from './storybook/ResizerStoryTemplate.vue';

export default {
  title: 'Shared/JResizer',
  argTypes: {
    minResize: {
      type: 'boolean',
      table: {
        category: 'Resizer',
      },
    },
    resizeBtn: {
      type: 'boolean',
      table: {
        category: 'Resizer',
      },
    },
    axisX: {
      type: 'boolean',
      table: {
        category: 'Resizer',
      },
    },
    axisY: {
      type: 'boolean',
      table: {
        category: 'Resizer',
      },
    },
  },
  args: {
    minResize: true,
    resizeBtn: true,
    axisX: true,
    axisY: true,
  },
};

const Template = (args) => ({
  components: { ResizerStoryTemplate },
  setup() {
    return {
      args,
    };
  },
  template: `
    <ResizerStoryTemplate v-bind="args" />
  `,
});

export const Base = Template.bind({});
