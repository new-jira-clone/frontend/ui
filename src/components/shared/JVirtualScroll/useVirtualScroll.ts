import {
  ComponentPublicInstance, computed, onBeforeMount, Ref, ref,
  watch,
} from 'vue';
import getElement from '@new-jira-clone/ui/src/composables/utils/getElementFromComponent';
import useCalcVirtualScrollUtils from './useCalcVirtualScrollUtils';

export default <T>(items: Ref<T[]>, rowHeight = 0, viewportPadding = 1) => {
  const viewportRef = ref<ComponentPublicInstance | HTMLElement | null>(null);
  const startNode = ref(0);
  const endNode = ref(0);
  const currentViewportHeight = ref(0);

  let viewportObserver: ResizeObserver | null = null;

  const {
    getEndNode,
    getStartNode,
    initWatcherForViewportHeight,
  } = useCalcVirtualScrollUtils();

  const totalContentHeight = computed(() => `${items.value.length * rowHeight}px`);

  const setViewportHeight = (height: number) => {
    currentViewportHeight.value = height;
  };

  const handleScroll = async () => {
    if (viewportRef.value === null) return;

    startNode.value = getStartNode(
      getElement(viewportRef.value),
      rowHeight,
      viewportPadding,
    );

    endNode.value = getEndNode(
      currentViewportHeight.value,
      rowHeight,
      startNode.value,
      viewportPadding,
    );
  };

  const visibleNodes = computed(() => {
    const newStartNode = startNode.value;
    const newEndNode = endNode.value;

    if (viewportRef.value === null) return [];

    return items.value.slice(newStartNode, newEndNode);
  });

  const offsetY = computed(() => `translate3d(0, ${startNode.value * rowHeight}px, 0`);

  watch(viewportRef, () => {
    viewportObserver?.disconnect();

    if (viewportRef.value) {
      viewportObserver = initWatcherForViewportHeight(getElement(viewportRef.value), (entry: ResizeObserverEntry) => {
        const { height } = entry.contentRect;

        setViewportHeight(Math.ceil(height));

        endNode.value = getEndNode(
          currentViewportHeight.value,
          rowHeight,
          startNode.value,
          viewportPadding,
        );
      });
    }
  });

  onBeforeMount(() => {
    viewportObserver?.disconnect();
  });

  return {
    items,
    offsetY,
    startNode,
    viewportRef,
    handleScroll,
    visibleNodes,
    totalContentHeight,
  };
};
