export default () => {
  const getStartNode = (viewport: HTMLElement, rowHeight: number, viewportPadding: number): number => {
    const scrollTop = viewport.scrollTop;

    return Math.max(0, Math.floor(scrollTop / rowHeight) - viewportPadding);
  };

  const getEndNode = (
    viewportHeight: number,
    rowHeight: number,
    startNode: number,
    viewportPadding: number,
  ) => Math.ceil(viewportHeight / rowHeight) + startNode + Math.ceil(viewportPadding * 2);

  const initWatcherForViewportHeight = (
    viewport: HTMLElement,
    observerCallback: (param: ResizeObserverEntry) => void,
  ): ResizeObserver => {
    const createdViewportObserver = new ResizeObserver((entries) => {
      // eslint-disable-next-line no-restricted-syntax
      for (const entry of entries) {
        observerCallback(entry);
      }
    });

    createdViewportObserver.observe(viewport);

    return createdViewportObserver;
  };

  return {
    getEndNode,
    getStartNode,
    initWatcherForViewportHeight,
  };
};
