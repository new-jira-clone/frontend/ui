import { ref } from 'vue';
import JVirtualScroll from './JVirtualScroll.vue';
import JList from '../../list/JList';
import JListItem from '../../list/JListItem';
import useVirtualScroll from './useVirtualScroll';
import JSpinner from '@/components/loaders/JSpinner';

export default {
  title: 'Shared/JVirtualScroll',
};

const Template = (args) => ({
  components: { JList, JListItem },
  setup() {
    const items = ref(new Array(5000).fill(0).map((_, index) => index + 1));

    const { offsetY, viewportRef, handleScroll, visibleNodes, totalContentHeight } =
      useVirtualScroll(items, 48);

    return {
      args,
      offsetY,
      handleScroll,
      visibleNodes,
      viewportRef,
      totalContentHeight,
    };
  },
  template: `
    <div
      ref="viewportRef"
      style="background-color: rgb(238, 238, 238); width: 250px; max-height: 90vh; overflow: auto;"
      @scroll="handleScroll"
    >
      <div :style="{ height: totalContentHeight }">
        <JList
          :style="{ transform: offsetY }"
        >
          <JListItem
            v-for="index in visibleNodes"
            :key="index"
          >
            Item {{ index }}
          </JListItem>
        </JList>
      </div>
    </div>
  `,
});

export const StaticHeight = Template.bind({});
