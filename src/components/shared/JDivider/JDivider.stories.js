import JDivider from './JDivider.vue';

export default {
  title: 'Shared/JDivider',
};

const Template = (args) => ({
  components: { JDivider },
  setup() {
    return {
      args,
    };
  },
  template: `
    <div>
      Base:
      <JDivider v-bind="args" />
    </div>
    <div>
      1px
      <JDivider height="1px" />
    </div>
    <div>
      2px
      <JDivider height="2px" />
    </div>
    <div>
      3px:
      <JDivider height="3px" />
    </div>
    <div>
      4px:
      <JDivider height="4px" />
    </div>
  `,
});

export const jDivider = Template.bind({});
jDivider.args = {
  height: '2px',
  marginTop: '16px',
  marginBottom: '16px',
};
