export default () => {
  let initialScrollTop = 0;
  let initialClickPosition = 0;

  const calcThumbSize = (scrollElement: HTMLElement, viewportHeight: number): number => {
    const areaSize = scrollElement.offsetHeight;

    const ratioViewport = (viewportHeight * viewportHeight) / areaSize;

    if (Math.floor(ratioViewport) === Math.floor(areaSize)) return 0;

    return Math.floor((viewportHeight * viewportHeight) / areaSize);
  };

  const calcThumbPosition = (
    containerScrollElement: HTMLElement,
    viewportHeight: number,
    thumbSize: number
  ): number => {
    const viewportScrollPos = containerScrollElement.scrollTop;

    return (viewportScrollPos / viewportHeight) * thumbSize;
  };

  const calcScrollTop = (
    containerScrollElement: HTMLElement,
    scrollElement: HTMLElement,
    thumbSize: number,
    clientY: number
  ): number => {
    const deltaY =
      (clientY - initialClickPosition) * (containerScrollElement.offsetHeight / thumbSize);

    const newScrollTop = Math.min(
      initialScrollTop + deltaY,
      scrollElement.offsetHeight - scrollElement.scrollTop
    );

    return newScrollTop;
  };

  const calcInitialScrollTop = (containerScrollElement: HTMLElement, clientY: number) => {
    initialClickPosition = clientY;
    initialScrollTop = containerScrollElement.scrollTop;
  };

  return {
    calcThumbSize,
    calcThumbPosition,
    calcScrollTop,
    calcInitialScrollTop,
  };
};
