import { ComponentPublicInstance, onUnmounted, provide, Ref, ref } from 'vue';
import getElement from '@new-jira-clone/ui/src/composables/utils/getElementFromComponent';
import useScrollCalculationUtils from './useScrollCalculationUtils';

export type IProvideScrollbar = {
  thumbScrollPosition: Ref<number>;
  thumbScrollSize: Ref<number>;
  onThumbDown: (event: MouseEvent) => void;
  onThumbMove: (event: MouseEvent) => void;
  scrollTo: (clickRatio: number) => void;
};

export const jScrollbarKey = 'jScrollbarKey';

export default (topBoundary?: Ref<number>, bottomBoundary?: Ref<number>) => {
  const viewportRef = ref<HTMLElement | null>(null);
  const contentRef = ref<HTMLElement | null>(null);
  const viewportHeight = ref(0);

  const thumbScrollPosition = ref(0);
  const thumbScrollSize = ref(0);

  let viewportObserver: ResizeObserver | null = null;
  let contentObserver: ResizeObserver | null = null;

  const { calcThumbSize, calcThumbPosition, calcInitialScrollTop, calcScrollTop } =
    useScrollCalculationUtils();

  const onViewportScroll = () => {
    requestAnimationFrame(() => {
      if (viewportRef.value === null) return;

      const newThumbPosition = calcThumbPosition(
        viewportRef.value,
        viewportHeight.value,
        thumbScrollSize.value
      );

      if (topBoundary !== undefined && newThumbPosition <= topBoundary.value) {
        thumbScrollPosition.value = topBoundary.value;
        return;
      }

      if (bottomBoundary !== undefined && newThumbPosition >= bottomBoundary.value) {
        thumbScrollPosition.value = bottomBoundary.value;
        return;
      }

      thumbScrollPosition.value = newThumbPosition;
    });
  };

  const initViewportHeight = () => {
    if (viewportRef.value === null) return;
    viewportHeight.value = viewportRef.value.offsetHeight;
  };

  const onThumbMove = (event: MouseEvent) => {
    if (viewportRef.value === null || contentRef.value === null) return;

    const containerElement = viewportRef.value;

    containerElement.scrollTop = calcScrollTop(
      containerElement,
      contentRef.value,
      thumbScrollSize.value,
      event.clientY
    );
  };

  const scrollTo = (clickRatio: number) => {
    if (contentRef.value === null || viewportRef.value === null) return;

    const scrollAmount = Math.floor(clickRatio * contentRef.value.scrollHeight);

    viewportRef.value.scrollTo({
      top: scrollAmount,
      behavior: 'smooth',
    });
  };

  const onThumbDown = (event: MouseEvent) => {
    if (viewportRef.value === null) return;

    calcInitialScrollTop(viewportRef.value, event.clientY);
  };

  const resizeviewportObserver = () => {
    if (viewportRef.value === null) return;

    viewportObserver = new ResizeObserver(() => {
      initViewportHeight();
      onViewportScroll();

      if (contentRef.value) {
        thumbScrollSize.value = calcThumbSize(contentRef.value, viewportHeight.value);
      }
    });

    viewportObserver.observe(viewportRef.value);
  };

  const resizeContentObserver = () => {
    if (contentRef.value === null) return;

    contentObserver = new ResizeObserver(() => {
      onViewportScroll();

      if (contentRef.value) {
        thumbScrollSize.value = calcThumbSize(contentRef.value, viewportHeight.value);
      }
    });

    contentObserver.observe(contentRef.value);
  };

  provide<IProvideScrollbar>(jScrollbarKey, {
    thumbScrollPosition,
    thumbScrollSize,
    onThumbDown,
    onThumbMove,
    scrollTo,
  });

  const initScrollbar = (
    newViewportRef: ComponentPublicInstance | HTMLElement,
    newContentRef: ComponentPublicInstance | HTMLElement,
  ) => {
    if (viewportRef.value && viewportObserver) {
      viewportObserver.disconnect();
    }

    if (viewportRef.value && contentObserver) {
      contentObserver.disconnect();
    }

    viewportRef.value = getElement(newViewportRef);
    contentRef.value = getElement(newContentRef);

    initViewportHeight();
    resizeviewportObserver();
    resizeContentObserver();

    if (contentRef.value) {
      thumbScrollSize.value = calcThumbSize(contentRef.value, viewportHeight.value);
    }
  };

  onUnmounted(() => {
    if (viewportObserver) {
      viewportObserver.disconnect();
    }

    if (contentObserver) {
      contentObserver.disconnect();
    }
  });

  return {
    initScrollbar,
    onViewportScroll,
  };
};
