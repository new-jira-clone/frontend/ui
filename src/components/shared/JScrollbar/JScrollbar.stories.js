import { onMounted, ref } from 'vue';
import JScrollbarTrack from './JScrollbarTrack.vue';
import JScrollbarContainer from './JScrollbarContainer';
import JScrollbarViewport from './JScrollbarViewport';
import JList from '../../list/JList';
import JListItem from '../../list/JListItem';
import useCustomScrollbar from './useCustomScrollbar';

export default {
  title: 'Shared/JScrollbar',
};

const Template = (args) => ({
  components: { JList, JListItem, JScrollbarTrack, JScrollbarContainer, JScrollbarViewport },
  setup() {
    const items = ref(new Array(100).fill(0).map((_, index) => index + 1));

    const scrollViewportRef = ref(null);
    const scrollContentRef = ref(null);

    const { onViewportScroll, initScrollbar } = useCustomScrollbar();

    const addItems = () => {
      items.value.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    };

    onMounted(() => {
      initScrollbar(scrollViewportRef.value, scrollContentRef.value)
    });

    return {
      initScrollbar,
      scrollViewportRef,
      scrollContentRef,
      onViewportScroll,
      args,
      addItems,
      items,
    };
  },
  template: `
    <button class="mb-4" @click="addItems">Add items</button>
    <JScrollbarContainer style="display: flex; position: relative; width: max-content;">
      <JScrollbarViewport
        ref="scrollViewportRef"
        style="position: relative; width: 250px; background-color: rgb(238, 238, 238); max-height: 90vh;"
        @scroll="onViewportScroll"
      >
        <JList
          ref="scrollContentRef"
        >
          <JListItem
            v-for="index in items"
            :key="index"
          >
            Item {{ index }}
          </JListItem>
        </JList>

      </JScrollbarViewport>
      <JScrollbarTrack v-bind="args" />
    </JScrollbarContainer>
  `,
});

export const jScrollbar = Template.bind({});
jScrollbar.args = {
  hideTrack: false,
  hideThumb: false,
};
