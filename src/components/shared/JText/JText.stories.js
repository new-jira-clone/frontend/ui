import JText, { textTypes, textColors, textWeight } from './JText.vue';

export default {
  title: 'Shared/JText',
  component: JText,
  argTypes: {
    type: {
      options: textTypes,
      control: { type: 'select' },
    },
    color: {
      options: textColors,
      control: { type: 'select' },
    },
    weight: {
      options: textWeight,
      control: { type: 'select' },
    },
  },
};

const Template = (args) => ({
  components: { JText },
  setup() {
    return { args };
  },
  template: `
    <JText v-bind="args">
      {{ args.text }}
    </JText>
  `,
});

const TemplateHeading = (args) => ({
  components: { JText },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: column;">
      <JText color="title" weight="weight" type="title-1">Title 1</JText>
      <JText color="title" weight="weight" type="title-3">Title 3</JText>
      <JText color="title" weight="weight" type="title-4">Title 4</JText>
      <JText color="title" weight="normal" type="text-1">Text 1</JText>
      <JText color="title" weight="normal" type="text-2">Text 2</JText>
      <JText color="title" weight="normal" type="text-3">Text 3</JText>
      <JText color="title" weight="normal" type="text-4">Text 4</JText>
      <JText color="title" weight="normal" type="text-5">Text 5</JText>
    </div>
  `,
});

const TemplateColors = (args) => ({
  components: { JText },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; flex-direction: column; background-color: rgba(241, 241, 241, 1);">
      <JText color="primary" weight="normal" type="title-4">Color primary</JText>
      <JText color="primary-text" weight="normal" type="title-4">Color primary-text</JText>
      <JText color="title" weight="normal" type="title-4">Color title</JText>
      <JText color="secondary-title" weight="normal" type="title-4">Color secondary-title</JText>
      <JText color="subtitle" weight="normal" type="title-4">Color subtitle</JText>
      <JText color="text" weight="normal" type="title-4">Color text</JText>
      <JText color="secondary-text" weight="normal" type="title-4">Color secondary-text</JText>
      <JText color="third-text" weight="normal" type="title-4">Color third-text</JText>
      <JText color="error" weight="normal" type="title-4">Color error</JText>
    </div>
  `,
});

export const Base = Template.bind({});
Base.args = {
  text: 'JText',
  type: 'text-1',
  color: 'text',
  weight: 'normal',
};

export const Heading = TemplateHeading.bind({});

export const Colors = TemplateColors.bind({});
