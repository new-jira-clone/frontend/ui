import JTeamCount from './JTeamCount.vue';

export default {
  title: 'Icons/JTeamCount',
  component: JTeamCount,
  args: {
    count: 3,
  },
};

const Template = (args) => ({
  components: { JTeamCount },
  setup() {
    return { args };
  },
  template: `
    <JTeamCount v-bind="args" />
  `,
});

export const jTeamCount = Template.bind({});
