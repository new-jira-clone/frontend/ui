import JActionIcon from './JActionIcon.vue';
import JIcon from '../JIcon/JIcon.stories';

export default {
  title: 'Icons/JActionIcon',
  component: JActionIcon,
  argTypes: {
    ...JIcon.argTypes,
    icon: {
      options: JIcon.icons,
      control: { type: 'select' },
    },
    size: {
      options: ['xs', 's'],
      control: { type: 'select' },
    },
  },
  args: {
    ...JIcon.args,
    icon: 'newLayer',
    size: 'xs',
  },
};

const Template = (args) => ({
  components: { JActionIcon },
  setup() {
    return { args };
  },
  template: `
    <JActionIcon>
      <JIcon v-bind="args" size="16px" />
    </JActionIcon>
  `,
});

export const jActionIcon = Template.bind({});
jActionIcon.args = {
  disable: false,
};
