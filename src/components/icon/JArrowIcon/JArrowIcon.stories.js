import JArrowIcon from './JArrowIcon.vue';

export default {
  title: 'Icons/JArrowIcon',
  component: JArrowIcon,
};

const Template = (args) => ({
  components: { JArrowIcon },
  setup() {
    return { args };
  },
  template: `
    <JArrowIcon v-bind="args" size="24px" />
  `,
});

export const jArrowIcon = Template.bind({});
jArrowIcon.args = {
  margin: false,
  containerSize: false,
  active: false,
};
