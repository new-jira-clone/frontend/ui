import JIcon from './JIcon.vue';
import iconsListConfig from './icons-list-config';

const iconsKeys = Object.keys(iconsListConfig);

const args = {
  size: '24px',
  iconWrapper: true,
  containerSize: true,
};

export default {
  title: 'Icons/JIcon',
  component: JIcon,
  icons: iconsKeys,
  argTypes: {
    iconWrapper: {
      options: [false, true],
      control: { type: 'radio' },
      table: {
        category: 'Icon',
      },
    },
    containerSize: {
      options: [false, true],
      control: { type: 'radio' },
      table: {
        category: 'Icon',
      },
    },
    size: {
      table: {
        category: 'Icon',
      },
    },
  },
  args,
};

const Template = (args) => ({
  components: { JIcon },
  data() {
    return {
      iconsList: iconsKeys,
    };
  },
  setup() {
    return { args };
  },
  template: `
  <div style="grid-template-columns: repeat(5, auto); display: grid; gap: 50px; background: #F5F5F5; padding: 20px">
    <div v-for="icon in iconsList" :key="icon" style="{ dispaly: flex; }">
      <JIcon v-bind="args" :icon="icon" />
      <div>name: <span class="text-subtitle-3 ml-1">{{ icon }}</span></div>
    </div>
  </div>
  `,
});

export const jIcon = Template.bind({});
