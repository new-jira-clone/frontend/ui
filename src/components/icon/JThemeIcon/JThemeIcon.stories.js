import JThemeIcon from './JThemeIcon.vue';

export default {
  title: 'Icons/JThemeIcon',
  component: JThemeIcon,
};

const Template = (args) => ({
  components: { JThemeIcon },
  setup() {
    return { args };
  },
  template: `
    <JThemeIcon theme="blue" />
    <JThemeIcon theme="purple" class="ml-2" />
  `,
});

export const jThemeIcon = Template.bind({});
