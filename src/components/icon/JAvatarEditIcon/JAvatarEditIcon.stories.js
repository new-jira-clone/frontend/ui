import JAvatarEditIcon from './JAvatarEditIcon.vue';

export default {
  title: 'Icons/JAvatarEditIcon',
  component: JAvatarEditIcon,
};

const Template = (args) => ({
  components: { JAvatarEditIcon },
  setup() {
    return { args };
  },
  template: `
    <JAvatarEditIcon v-bind="args" />
  `,
});

export const jAvatarEditIcon = Template.bind({});
jAvatarEditIcon.args = {
  margin: false,
  containerSize: false,
  active: false,
};
