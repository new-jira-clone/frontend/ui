import { type Directive, type DirectiveBinding } from 'vue';

const elsHandlers = new WeakMap();

let handleOutsideClick: (event: MouseEvent) => void;

const isExcludeElement = (target: HTMLElement, exclude: HTMLElement): boolean => {
  if (Array.isArray(exclude)) {
    const isExcludeItem = exclude.some((excludeItem: HTMLElement): boolean => {
      return target === excludeItem || excludeItem.contains(target);
    });

    return isExcludeItem;
  }

  return target === exclude;
};

const onBind: Directive = (el: HTMLElement, binding: DirectiveBinding): void => {
  const { handler, exclude } = binding.value;

  if (el && exclude && handler) {
    elsHandlers.set(el, binding.value);
  }

  handleOutsideClick = (event: MouseEvent): void => {
    if (!el || !elsHandlers.get(el)) return;

    const { handler, exclude } = elsHandlers.get(el);

    const target = event.target as HTMLElement;

    if (isExcludeElement(target, exclude)) return;
    if (el.contains(target) === false) {
      handler?.();
    }
  };

  document.addEventListener('click', handleOutsideClick);
};

const onUpdate: Directive = (el: HTMLElement, binding: DirectiveBinding): void => {
  const { handler, exclude } = binding.value;

  if (el && exclude && handler) {
    elsHandlers.set(el, binding.value);
  }
};

const onUnbind: Directive = (el: HTMLElement): void => {
  elsHandlers.delete(el);
  document.removeEventListener('click', handleOutsideClick);
};

export const ClickOutside = {
  directiveName: 'closable',
  created: onBind,
  updated: onUpdate,
  unmounted: onUnbind,
};
