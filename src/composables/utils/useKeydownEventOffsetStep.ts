export default () => {
  const keysdown = {
    up: false,
    right: false,
    down: false,
    left: false,
  };

  const STEP_OFFSET = 10;

  const getOffsetStepOnKeyboardEvent = (event: KeyboardEvent): [number, number] => {
    let xOffset = 0;
    let yOffset = 0;

    if (event.code === 'ArrowUp' || keysdown.up) {
      keysdown.up = true;

      yOffset -= STEP_OFFSET;
    }

    if (event.code === 'ArrowRight' || keysdown.right) {
      keysdown.right = true;

      xOffset += STEP_OFFSET;
    }

    if (event.code === 'ArrowDown' || keysdown.down) {
      keysdown.down = true;

      yOffset += STEP_OFFSET;
    }

    if (event.code === 'ArrowLeft' || keysdown.left) {
      keysdown.left = true;

      xOffset -= STEP_OFFSET;
    }

    return [xOffset, yOffset];
  };

  const resetKeydownState = (event: KeyboardEvent) => {
    switch (event.code) {
      case 'ArrowUp':
        keysdown.up = false;
        break;
      case 'ArrowRight':
        keysdown.right = false;
        break;
      case 'ArrowDown':
        keysdown.down = false;
        break;
      case 'ArrowLeft':
        keysdown.left = false;
        break;
    }
  };

  return {
    resetKeydownState,
    getOffsetStepOnKeyboardEvent,
  };
};
