import { computed } from 'vue';

const themeTypes = {
  default: 'light-blue',
  lightPurple: 'light-purple',
};

const lcThemeKey = 'currentTheme';

export default () => {
  const isExistTheme = (theme: string): boolean => {
    const allThemes = Object.values(themeTypes);
    return allThemes.some((iteratorTheme: string): boolean => iteratorTheme === theme);
  };

  const setTheme = (theme: string): void => {
    const allThemes = Object.values(themeTypes);
    const htmlElement: HTMLElement | null = document.querySelector('html');

    allThemes.forEach((iteratorTheme: string): void => {
      htmlElement?.classList.remove(iteratorTheme);
    });

    htmlElement?.classList.add(theme);
    localStorage.setItem(lcThemeKey, theme);
  };

  const initTheme = (): void => {
    const selectedTheme: string | null = localStorage.getItem(lcThemeKey);

    if (selectedTheme && isExistTheme(selectedTheme)) {
      setTheme(selectedTheme);
    } else {
      setTheme(themeTypes.default);
    }
  };

  const getCurrentTheme = computed(() => {
    const selectedTheme: string | null = localStorage.getItem(lcThemeKey);

    if (selectedTheme && isExistTheme(selectedTheme)) {
      return selectedTheme;
    }

    return '';
  });

  return {
    themeTypes,
    initTheme,
    setTheme,
    getCurrentTheme,
  };
};
