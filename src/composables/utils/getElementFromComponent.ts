import { ComponentPublicInstance } from 'vue';

export default (elementOrComponent: ComponentPublicInstance | HTMLElement): HTMLElement => {
  if ('$el' in elementOrComponent) {
    return elementOrComponent.$el;
  }

  return elementOrComponent;
};
