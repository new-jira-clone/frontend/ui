export default function debounce(callback: any, timeout: number = 300): (...args: any) => void {
  let timer: number | undefined;

  return function executedFunction(...args) {
    const later = () => {
      clearTimeout(timer);
      callback(...args);
    };

    clearTimeout(timer);
    timer = window.setTimeout(later, timeout);
  };
}
