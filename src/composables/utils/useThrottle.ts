export interface ThrottleOptions {
  force: boolean;
}

const defaultOptons = {
  force: false,
};

export default function throttle(
  callback: (...args: any) => void,
  timeout: number,
  options: ThrottleOptions = defaultOptons,
): (...args: any) => void {
  let timer: number | null = null;
  let isForced = false;

  return function perfrom(...args): void {
    if (timer) return;

    if (options.force && isForced === false) {
      callback(...args);
      isForced = true;

      timer = window.setTimeout(() => {
        if (timer) {
          clearTimeout(timer);
          timer = null;
        }
      }, timeout);

      return;
    }

    timer = window.setTimeout(() => {
      callback(...args);

      if (timer) {
        clearTimeout(timer);
        timer = null;
      }
    }, timeout);
  };
}
