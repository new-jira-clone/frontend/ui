import {
  Component, markRaw, nextTick, ref, ShallowRef, Ref,
} from 'vue';

export type DialogType = 'modal' | 'nonModal';

export interface DialogOptions {
  externalId?: string;
  dialogProps?: any;
  contentProps?: any;
}

export interface DynamicDialog {
  id: number;
  zIndex: number;
  component: Component;
  options: DialogOptions;
  type: DialogType;
  model: boolean;
}

interface Store {
  id: Ref<number>;
  dynamicDialogs: ShallowRef<DynamicDialog[]>;
  nonModalDialogsCount: Ref<number>;
}

const state: Store = {
  id: ref(0),
  dynamicDialogs: ref([]),
  nonModalDialogsCount: ref(0),
};

export const useDialogsStore = () => {
  const addDynamicDialog = async (
    dialog: Component,
    type: DialogType,
    options: any,
    zIndex: number,
  ): Promise<number> => {
    state.id.value += 1;

    const newDialogConfig = {
      id: state.id.value,
      zIndex,
      component: markRaw(dialog),
      options,
      type,
      model: false,
    };

    state.dynamicDialogs.value.push(newDialogConfig);

    /** nextTick is required for the dialog appearance animation  */
    await nextTick();

    state.dynamicDialogs.value[state.dynamicDialogs.value.length - 1].model = true;

    return newDialogConfig.id;
  };

  const removeDynamicDialog = async (dynamicDialog: DynamicDialog): Promise<void> => {
    /** nextTick is required for the dialog close animation  */
    await nextTick();

    state.dynamicDialogs.value = state.dynamicDialogs.value.filter(
      (savedDialog) => savedDialog !== dynamicDialog,
    );
  };

  const removeDynamicDialogById = async (dialogId: number): Promise<void> => {
    const removedDialog = state.dynamicDialogs.value.find(
      (savedDialog) => savedDialog.id === dialogId,
    );

    if (removedDialog) {
      removedDialog.model = false;
    }

    /** nextTick is required for the dialog close animation  */
    await nextTick();

    state.dynamicDialogs.value = state.dynamicDialogs.value.filter(
      (savedDialog) => savedDialog.id !== dialogId,
    );
  };

  const removeLastDynamicDialog = async (type: DialogType): Promise<void> => {
    let lastIndex = -1;

    // TODO: refactor when ts version >= 5.3.3
    for (let index = state.dynamicDialogs.value.length - 1; index >= 0; index -= 1) {
      if (type === state.dynamicDialogs.value[index].type) {
        lastIndex = index;
        break;
      }
    }

    if (lastIndex === -1) return;

    state.dynamicDialogs.value[lastIndex].model = false;

    /** nextTick is required for the Dialog close animation  */
    await nextTick();

    state.dynamicDialogs.value.pop();
  };

  const resetDialogs = async (type: DialogType): Promise<void> => {
    for (let index = 0; index >= 0; index -= 1) {
      if (state.dynamicDialogs.value[index].type === type) {
        state.dynamicDialogs.value[index].model = false;
      }
    }

    /** nextTick is required for the dialog close animation  */
    await nextTick();

    state.dynamicDialogs.value = [];
  };

  const increaseNonModalsCount = () => {
    state.nonModalDialogsCount.value += 1;
  };

  const activeNonModalDialogs = () => {
    return state.dynamicDialogs.value.filter((dynamicDialog) => dynamicDialog.type === 'nonModal');
  };

  const numberOpenedNonModalDialogs = () => state.nonModalDialogsCount.value;

  const resetNumberOpenedModalDialogs = () => {
    state.nonModalDialogsCount.value = 0;
  };

  const findByExternalId = (externalId: string): DynamicDialog | undefined => {
    return state.dynamicDialogs.value.find(
      (dynamicDialog) => dynamicDialog.options.externalId === externalId,
    );
  };

  return {
    resetDialogs,
    addDynamicDialog,
    removeDynamicDialog,
    removeDynamicDialogById,
    removeLastDynamicDialog,
    dynamicDialogs: state.dynamicDialogs,
    increaseNonModalsCount,
    numberOpenedNonModalDialogs,
    resetNumberOpenedModalDialogs,
    activeNonModalDialogs,
    findByExternalId,
  };
};
