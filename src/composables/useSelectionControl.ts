export const forbidSelection = async (additionalClass = ''): Promise<void> => {
  const html = document.querySelector('html');

  if (additionalClass) {
    html?.classList.add('forbid-selection', additionalClass);
  } else {
    html?.classList.add('forbid-selection');
  }
};

export const allowSelection = async (additionalClass = ''): Promise<void> => {
  const html = document.querySelector('html');

  if (additionalClass) {
    html?.classList.remove('forbid-selection', additionalClass);
  } else {
    html?.classList.remove('forbid-selection');
  }
};
