import {
  Component, ref, Ref, type App,
} from 'vue';
import {
  useDialogsStore,
  DialogOptions,
} from '@new-jira-clone/ui/src/composables/store/useDialogsStore';

export interface DialogPlugin {
  showModal: (component: Component, options?: DialogOptions) => Promise<number>;
  closeModal: () => void;
  closeAllModals: () => void;
  show: (component: Component, options?: DialogOptions) => Promise<number>;
  close: (dialogId: number) => void;
  closeAll: () => void;
  getInitialNonModalDialogPosition: () => { top: number; left: number };
  increaseZIndex: () => void;
  globalZIndex: Ref<number>;
}

const globalZIndex = ref(1000);

const init = (): DialogPlugin => {
  const increaseZIndex = (): void => {
    globalZIndex.value += 1;
  };

  const showModal = async (component: Component, options: DialogOptions = {}): Promise<number> => {
    const { addDynamicDialog } = useDialogsStore();

    increaseZIndex();

    return addDynamicDialog(component, 'modal', options, globalZIndex.value);
  };

  const show = async (component: Component, options: DialogOptions = {}): Promise<number> => {
    const { addDynamicDialog, increaseNonModalsCount, findByExternalId } = useDialogsStore();

    increaseZIndex();

    if (options.externalId) {
      const existDynamicDialog = findByExternalId(options.externalId);

      if (existDynamicDialog) {
        existDynamicDialog.zIndex = globalZIndex.value;

        return existDynamicDialog.id;
      }
    }

    increaseNonModalsCount();

    return addDynamicDialog(component, 'nonModal', options, globalZIndex.value);
  };

  const closeModal = (): void => {
    const { removeLastDynamicDialog } = useDialogsStore();

    removeLastDynamicDialog('modal');
  };

  const close = async (dialogId: number): Promise<void> => {
    const { removeDynamicDialogById, activeNonModalDialogs, resetNumberOpenedModalDialogs } =
      useDialogsStore();

    await removeDynamicDialogById(dialogId);

    const numberActiveNonModalDialogs = await activeNonModalDialogs();

    if (numberActiveNonModalDialogs.length === 0) {
      resetNumberOpenedModalDialogs();
    }
  };

  const closeAllModals = (): void => {
    const { resetDialogs } = useDialogsStore();

    resetDialogs('modal');
  };

  const closeAll = (): void => {
    const { resetDialogs } = useDialogsStore();

    resetDialogs('nonModal');
  };

  /** Simple settings position realization to non modal dialogs */
  const getInitialNonModalDialogPosition = (
    offsetX = 50,
    offsetY = 50,
  ): { top: number; left: number } => {
    const { numberOpenedNonModalDialogs, resetNumberOpenedModalDialogs } = useDialogsStore();

    const MAX_NUMBER_DIALOGS_TO_FIRST_WINDOW = 5;
    const MAX_NUMBER_DIALOGS_TO_SECOND_WINDOW = 10;

    const PERCENT_OF_DOCUMENT_SIZE_FOR_FIRST_WINDOW = 0.1;
    const PERCENT_OF_DOCUMENT_SIZE_FOR_SECOND_WINDOW = 0.3;

    const openedNonModalDialogs = numberOpenedNonModalDialogs();

    const initialTopMultiplier =
      openedNonModalDialogs > MAX_NUMBER_DIALOGS_TO_FIRST_WINDOW
        ? PERCENT_OF_DOCUMENT_SIZE_FOR_SECOND_WINDOW
        : PERCENT_OF_DOCUMENT_SIZE_FOR_FIRST_WINDOW;

    const offsetMultiplier =
      openedNonModalDialogs > MAX_NUMBER_DIALOGS_TO_FIRST_WINDOW
        ? openedNonModalDialogs % (MAX_NUMBER_DIALOGS_TO_FIRST_WINDOW + 1)
        : openedNonModalDialogs;

    const initialTop = document.documentElement.clientHeight * initialTopMultiplier;
    const initialLeft =
      document.documentElement.clientWidth * PERCENT_OF_DOCUMENT_SIZE_FOR_FIRST_WINDOW;

    if (openedNonModalDialogs >= MAX_NUMBER_DIALOGS_TO_SECOND_WINDOW) {
      resetNumberOpenedModalDialogs();
    }

    return {
      top: Math.floor(initialTop + offsetX * offsetMultiplier),
      left: Math.floor(initialLeft + offsetY * offsetMultiplier),
    };
  };

  return {
    showModal,
    closeModal,
    closeAllModals,
    show,
    close,
    closeAll,
    getInitialNonModalDialogPosition,
    increaseZIndex,
    globalZIndex,
  };
};

export default {
  install(app: App) {
    app.config.globalProperties.$dialog = init();

    app.provide('$dialog', app.config.globalProperties.$dialog);
  },
};
