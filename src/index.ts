import * as components from './components';
import * as directives from './directives';
import * as install from './install';

export {
  components,
  directives,
  install,
};
