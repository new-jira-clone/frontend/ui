import { type Preview, setup } from '@storybook/vue3';
import { createUi } from '@/install';
import dialog from '@/plugins/dialog.ts';
import * as directives from '@/directives';
import useTheme from '@/composables/utils/useTheme';
import { JApp } from '@new-jira-clone/ui/src/components';
import * as UIComponents from '@/components';
import MouseEventTool from './MouseEventTool.vue';
import Icons from '../assets/icons.svg?raw';

import '../styles.scss';
import './styles.scss';
import '@new-jira-clone/ui/styles.scss';

const { themeTypes, setTheme } = useTheme();

setup((app) => {
  app.use(createUi({ directives, svgSprite: { raw: Icons } }));
  app.use(dialog);

  Object.values(UIComponents).forEach((UIComponent) => {
    if (UIComponent.name) {
      app.component(UIComponent.name, UIComponent);
    }
  });

  app.component('MouseEventTool', MouseEventTool);
});

const preview: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  globalTypes: {
    theme: {
      name: 'Theme',
      description: 'Theme for the components',
      defaultValue: 'default',
      toolbar: {
        icon: 'circlehollow',
        items: [
          { value: 'default', icon: 'circlehollow', title: 'light-blue' },
          { value: 'lightPurple', icon: 'circle', title: 'light-purple' },
        ],
        showName: true,
      },
    },
  },
  decorators: [
    (ctx, params) => {
      setTheme(themeTypes[params.globals.theme]);
      return {
        template: '<JApp /> <story /> <MouseEventTool />',
      };
    },
  ],
};

export default preview;
