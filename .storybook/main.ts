import type { StorybookConfig } from '@storybook/vue3-vite';
const path = require('path');

const config: StorybookConfig = {
  staticDirs: ['../public'],

  stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)', '../src/components/animation/JTypingDots/JTypingDots.stories.js'],

  addons: [
    '@storybook/addon-onboarding',
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@chromatic-com/storybook',
    '@storybook/addon-interactions',
    '@chromatic-com/storybook',
  ],

  framework: {
    name: '@storybook/vue3-vite',
    options: {},
  },

  docs: {
    autodocs: false,
  },

  viteFinal: async (config) => {
    if (config.resolve?.alias) {
      config.resolve.alias['@'] = path.resolve(__dirname, '../src/');
    }

    return config;
  },
};

export default config;
